import { StyleSheet } from "react-native";
import Colors from "../../assets/colors/colors";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch"
  },
  input: {
    textAlign: "center",
    alignSelf: "center",
    paddingVertical: 10,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.blueUFF,
    marginBottom: 18,
    height: 35,
    width: "80%"
  },
  img: {
    marginBottom: 67,
    alignSelf: "center"
  }
});

export default styles;
