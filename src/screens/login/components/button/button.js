import React from 'react';
import {TouchableOpacity,Text} from 'react-native';
import Styles from './style'

const button = () => {
  return (
    <TouchableOpacity style={Styles.btn}>
      <Text style={Styles.btnText}>Entrar</Text>
    </TouchableOpacity>
  )
}

export default button
