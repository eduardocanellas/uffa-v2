import {StyleSheet} from 'react-native';
import Colors from '../../../../assets/colors/colors'

const styles = StyleSheet.create({
  btn:{
    alignSelf:"center",
    backgroundColor:Colors.blueUFF,
    borderRadius:5,
    marginTop:50,
  },
  btnText:{
    color:Colors.primaryWhite,
    textTransform:"uppercase",
    paddingVertical:10,
    paddingHorizontal:22, 
  }
});

export default styles;