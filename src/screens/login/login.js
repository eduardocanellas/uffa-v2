import React from "react";
import { ScrollView, Image, TextInput, View } from "react-native";
import Button from "./components/button/button";
import Style from "./style";

export default function HomeScreen() {
  return (
    <ScrollView contentContainerStyle={Style.container}>
      <Image
        style={Style.img}
        source={require("../../assets/images/logo.png")}
      />
      <View style={Style.inputContainer}>
        <TextInput style={Style.input} placeholder="CPF" />
        <TextInput
          style={Style.input}
          secureTextEntry={true}
          placeholder="Senha"
        />
      </View>
      <Button />
    </ScrollView>
  );
}

HomeScreen.navigationOptions = {
  header: null
};
