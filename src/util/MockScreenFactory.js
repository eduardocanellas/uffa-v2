import React from "react";
import { View, Text } from "react-native";

const MockScreenFactory = mockName => {
  return () => (
    <View>
      <Text>{mockName}</Text>
    </View>
  );
};

export default MockScreenFactory;
