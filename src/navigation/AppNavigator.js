import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";

import AuthNavigator from "./AuthNavigator";
import CommunityNavigator from "./CommunityNavigator";
import SupervisorNavigator from "./SupervisorNavigator";
import WorkerNavigator from "./WorkerNavigator";

const MockScreen = props => {
  const navigate = stack => () => props.navigation.navigate(stack);
  return (
    <View style={{flex: 1, marginTop: 50}}>
      <TouchableOpacity onPress={navigate("Auth")}>
        <Text>Auth</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigate("Community")}>
        <Text>Community</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigate("Supervisor")}>
        <Text>Supervisor</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigate("Worker")}>
        <Text>Worker</Text>
      </TouchableOpacity>
    </View>
  );
};

export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Main: MockScreen,
    Auth: AuthNavigator,
    Community: CommunityNavigator,
    Supervisor: SupervisorNavigator,
    Worker: WorkerNavigator
  })
);
