import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';

import LoginScreen from '../screens/login/login';

const CommunityStack = createStackNavigator({
  Main: LoginScreen,
});

CommunityStack.navigationOptions = {
  tabBarLabel: 'Comunidade',
  tabBarIcon: ({focused}) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

CommunityStack.path = '';

export default CommunityStack;
