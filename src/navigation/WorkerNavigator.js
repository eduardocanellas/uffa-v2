import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {Platform} from 'react-native';
import TabBarIcon from '../components/TabBarIcon';

import TaskListScreen from '../modules/worker/screens/TaskListScreen';
import TaskScreen from '../modules/worker/screens/TaskScreen';

import mockScreen from '../util/MockScreenFactory';

const TaskStack = createStackNavigator({
  Main: TaskListScreen,
  Task: TaskScreen,
});

TaskStack.navigationOptions = {
  tabBarLabel: 'Tarefas',
  tabBarIcon: ({focused}) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

TaskStack.path = '';

const AnotherStack = createStackNavigator({
  Main: mockScreen('Worker AnotherStack'),
});

AnotherStack.navigationOptions = {
  tabBarLabel: 'Outros',
  tabBarIcon: ({focused}) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const WorkerNavigation = createBottomTabNavigator({TaskStack, AnotherStack});

export default WorkerNavigation;
