import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import TaskListScreen from "../modules/community/screens/TaskListScreen";
import ChatScreen from "../modules/community/screens/ChatScreen";

const CommunityStack = createStackNavigator({
  Chat: ChatScreen,
  Main: TaskListScreen
});

CommunityStack.navigationOptions = {
  tabBarLabel: "Comunidade",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === "ios"
          ? `ios-information-circle${focused ? "" : "-outline"}`
          : "md-information-circle"
      }
    />
  )
};

CommunityStack.path = "";

export default CommunityStack;
