import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {Platform} from 'react-native';
import TabBarIcon from '../components/TabBarIcon';

import TaskListScreen from '../modules/supervisor/screens/TaskListScreen';
import TaskScreen from '../modules/supervisor/screens/TaskScreen';
import TaskEditScreen from '../modules/supervisor/screens/TaskEditScreen';

import mockScreen from '../util/MockScreenFactory';

const TaskStack = createStackNavigator({
  Main: TaskListScreen,
  Task: TaskScreen,
  TaskEdit: TaskEditScreen,
});

TaskStack.navigationOptions = {
  tabBarLabel: 'Tarefas',
  tabBarIcon: ({focused}) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

TaskStack.path = '';

const AnotherStack = createStackNavigator({
  Main: mockScreen('Supervisor AnotherStack'),
});

AnotherStack.navigationOptions = {
  tabBarLabel: 'Outros',
  tabBarIcon: ({focused}) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const SupervisorNavigator = createBottomTabNavigator({
  TaskStack,
  AnotherStack,
});

export default SupervisorNavigator;
