const colors = {
  blueUFF: "#0F62AC",
  primaryWhite: "#fafafa",
  primaryBlack: "#333333",
  cardColor: "#FFFFFF",
  shadow: "#000"
};

export default colors;
