import React from "react";
import { FlatList, View, Text, Image } from "react-native";
import { TextInput, ScrollView } from "react-native-gesture-handler";
import LinearGradient from "react-native-linear-gradient";
import Style from "./styles/taskListStyle";
import Card from "../../components/card/card";
import SearchBar from "../../components/searchBar/searchBar";
import SafeAreaView from "react-native-safe-area-view";

const TaskListScreen = () => {
  const handleToLogout = () => {};

  return (
    <SafeAreaView style={Style.list}>
      <ScrollView>
        <SearchBar onPress={handleToLogout} />
        <Text style={Style.titleSection}>Solicitações abertas</Text>
        <Card />
        <Text style={Style.titleSection}>Solicitações fechadas</Text>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TaskListScreen;
