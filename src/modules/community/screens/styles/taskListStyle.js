import React from "react";
import { StyleSheet } from "react-native";
import Colors from "../../../../assets/colors/colors";

const style = StyleSheet.create({
  titleSection: {
    fontSize: 13,
    color: Colors.primaryBlack,
    marginTop: 12,
    marginLeft: 20
  },
  list: {
    flex: 1
  }
});

export default style;
