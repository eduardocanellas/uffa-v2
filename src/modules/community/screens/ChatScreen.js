import React from "react";
import { View, Text, Image } from "react-native";
import {
  TextInput,
  TouchableOpacity,
  ScrollView
} from "react-native-gesture-handler";
import Style from "./styles/chatStyle";

const ChatScreen = () => {
  return (
    <View>
      <Text>Chat</Text>
      <ScrollView contentContainerStyle={Style.container}></ScrollView>
      <TextInput placeholder="Digite aqui" />
      <TouchableOpacity style={Style.btn}>
        <Image />
      </TouchableOpacity>
    </View>
  );
};

export default ChatScreen;
