import React from "react";
import { View, Text } from "react-native";
import Style from "./style";

const card = () => {
  return (
    <View style={Style.card}>
      <View style={Style.headerCard}>
        <Text>Vazamento de Esgoto</Text>
        <Text>00/00/00</Text>
      </View>
      <Text style={Style.description}>
        A nível organizacional, o desafiador cenário globalizado estimula a
        padronização do processo de comunicação como um todo.
      </Text>
      <Text style={Style.local}>Instituto de Computação</Text>
      <Text style={Style.status}>Atribuido à funcionário</Text>
    </View>
  );
};

export default card;
