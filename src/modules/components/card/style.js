import { StyleSheet } from "react-native";
import Colors from "../../../assets/colors/colors";

const style = StyleSheet.create({
  card: {
    height: 124,
    width: "80%",
    alignSelf: "center",
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    shadowColor: Colors.shadow,
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 5,
    borderColor: "#ddd"
  },
  shadow: {},
  headerCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10
  },
  description: {
    fontSize: 10,
    color: Colors.primaryBlack
  },
  local: {
    fontSize: 12,
    marginTop: 7
  },
  status: {
    fontSize: 10,
    color: Colors.blueUFF,
    alignSelf: "flex-end"
  }
});

export default style;
