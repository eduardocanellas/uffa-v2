import React from "react";
import { View, TextInput, Image } from "react-native";
import Style from "./style";

const searchBar = props => {
  return (
    <View style={Style.searchBar}>
      <Image
        style={Style.logoutBtn}
        onPress={props.onPress}
        source={require("../../../assets/images/logoutBtn.png")}
      />
      <TextInput style={Style.input} placeholder="Busque aqui..."></TextInput>
    </View>
  );
};

export default searchBar;
