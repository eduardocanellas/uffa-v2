import React from "react";
import { StyleSheet } from "react-native";
import Colors from "../../../assets/colors/colors";

const style = StyleSheet.create({
  searchBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 20,
    marginLeft: 20
  },
  logoutBtn: {},
  input: {
    borderColor: Colors.blueUFF,
    borderWidth: 1,
    borderRadius: 5,
    width: "70%",
    paddingLeft: 10,
    paddingVertical: 9
  }
});

export default style;
